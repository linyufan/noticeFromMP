# 通知效果：

- ![输入图片说明](https://foruda.gitee.com/images/1666342928180064548/ba8cc88a_2017801.jpeg "微信图片_20221021170010.jpg")


## 一、在微信公众平台，申请测试号：

https://mp.weixin.qq.com/debug/cgi-bin/sandbox?t=sandbox/login



## 二、填写接口配置信息

URL地址写你博客接口文件的地址，如：
https://www.linyufan.com/xxx/wechat.index.php

Token你自己随便写一个，不在PHP端校验的话，这个就没用。

![输入图片说明](snipaste_20221021_161059.png)



## 三、关注这个临时测试微信公众号

记下这里显示的微信号，其实就是你这个微信号对应公众号的openid。

![输入图片说明](snipaste_20221021_162352.png)





## 四、填写消息接口的模板

（1）模板标题随便写，我写的是“大王！来新评论了”。效果如下：
![输入图片说明](https://foruda.gitee.com/images/1666342895651572219/a5308461_2017801.jpeg "微信图片_20221021170014.jpg")

（2）模板内容可以参考我的：

用户：{{username.DATA}} 标题：{{comm_title.DATA}} 留言：{{comm_content.DATA}} 时间：{{comm_time.DATA}}

![输入图片说明](snipaste_20221021_162840.png)



## 五、将文件上传到xxx目录

### 1、wechat.index.php 文件中需要修改：
（1）、22行左右，$openid = '这里是你的openid';


### 2、WxTmpLinyufan.class.php 文件中需要修改的：

（1）、26行左右的//接口URL

把里面的 secret 和 appid 写成你的

（2）、55行左右，把第四步生成的模板id，写到这里。'template_id' => '这里填写你生成的模板id',//模板ID

## 六、评论程序中使用

使用方法1：

在评论提交成功后，通过URL带参数的形式访问：

```

$comm_weixin = 'https://www.linyufan.com/xxx/wechat.index.php?username='.urlencode($data['username']).'&comm_title=《'.urlencode($title).'》&comm_content='.urlencode($data['content']);
https_request($comm_weixin);
```



```
function https_request($url){
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_REFERER, $url);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
	curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36');
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
	curl_setopt($ch, CURLOPT_POST, true); //2021.3.6 小程序发邮件
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	$results = curl_exec($ch);
	curl_close($ch);
	return $results;
}
```

使用方法2：

把wechat.index.php文件当接口文件，改造改造，写一个带返回值的函数，在评论提交成功后，调用。


注意：接口使用的地址必须是ssl的，也就是访问得是https的

