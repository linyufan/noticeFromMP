<?php
/**
 * 微信公众平台操作类
 */
class WxTmpLinyufan
{
    //请求模板消息的地址
    const TEMP_URL = 'https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=';
    // 获取access_token
    /**
     * 获取 access_tonken值
     * @param string $token_file 用来存储的文件
     * @return access_token
     */
    
    //这里为了应对更多程序的使用情况，我把 access_token 写到text中了
    //如果你对自己的博客系统比较熟悉，可以写到博客缓存中，默认是2两个小时有效
    //access_token是访问微信公众平台接口的重要凭证，不能乱改，也不能乱丢
    public function getAccessToken($token_file='access_token.txt'){
        //处理是否过期问题，将access_token存储到文件
        $life_time = 7200;
        if (file_exists($token_file) && time() - filemtime($token_file) < $life_time) {
            // 存在有效的access_token 直接返回文件内容
            return file_get_contents($token_file);
        }
        //接口URL
        $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=这个是你的微信公众号的appid&secret=这个是你微信公众号的appsecret";
        //发送GET请求
        $result = $this->_request('get',$url);
        if (!$result) {
            return false;
        }
        //处理数据
        $result_obj = json_decode($result);
        //写入到文件
        file_put_contents($token_file, $result_obj->access_token);
        return $result_obj->access_token;
    }





    /**
    * 微信模板消息发送
    * @param $openid 接收用户的openid
    * return 发送结果
    */
    public function send($openid,$username,$comm_title,$comm_content,$comm_time){
        $tokens = $this->getAccessToken();
        //var_dump($tokens);exit;
        $url = self::TEMP_URL . $tokens;
        $params = [
            'touser' => $openid,
            'template_id' => '这里填写你生成的模板id',//模板ID
            'url' => 'https://www.linyufan.com/', //点击详情后的URL可以动态定义
            'data' => 
                    [
                      'username' => 
                         [
                            'value' => $username,
                            'color' => '#173177'
                         ],
                        'comm_title' => 
                         [
                            'value' => $comm_title,
                            'color' => '#173177'
                         ],

                      'comm_content' => 
                         [
                                'value' => $comm_content,
                                'color' => '#173177'
                         ],
                       'comm_time' => 
                         [
                                'value' => $comm_time,
                                'color' => '#173177'
                         ] 
                      ]
        ]; 
        $json = json_encode($params,JSON_UNESCAPED_UNICODE);
        return $this->curlPost($url, $json);
    }
    /**
    * 通过CURL发送数据
    * @param $url 请求的URL地址
    * @param $data 发送的数据
    * return 请求结果
    */
    protected function curlPost($url,$data){
        $ch = curl_init();
        $params[CURLOPT_URL] = $url;    //请求url地址
        $params[CURLOPT_HEADER] = FALSE; //是否返回响应头信息
        $params[CURLOPT_SSL_VERIFYPEER] = false;
        $params[CURLOPT_SSL_VERIFYHOST] = false;
        $params[CURLOPT_RETURNTRANSFER] = true; //是否将结果返回
        $params[CURLOPT_POST] = true;
        $params[CURLOPT_POSTFIELDS] = $data;
        curl_setopt_array($ch, $params); //传入curl参数
        $content = curl_exec($ch); //执行
        curl_close($ch); //关闭连接
        return $content;
    }




    /**
     * 封装发送http请求
     * @param string $method 请求方式 get/post
     * @param string $url 请求目标的url
     * @param array $data post发送的数据
     * @param bool $ssl 是否为https协议
     * @return 响应主体
     */
    private function _request($method='get',$url,$data=array(),$ssl=true){
        //curl完成，先开启curl模块
        //初始化一个curl资源
        $curl = curl_init();
        //设置curl选项
        curl_setopt($curl,CURLOPT_URL,$url);//url
        //请求的代理信息
        $user_agent = isset($_SERVER['HTTP_USER_AGENT'])?$_SERVER['HTTP_USER_AGENT']: 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0 FirePHP/0.7.4';
        curl_setopt($curl,CURLOPT_USERAGENT,$user_agent);
        //referer头，请求来源        
        curl_setopt($curl,CURLOPT_AUTOREFERER,true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);//设置超时时间
        //SSL相关
        if($ssl){
            //禁用后，curl将终止从服务端进行验证;
            curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,false);
            //检查服务器SSL证书是否存在一个公用名
            curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,2);
        }
        //判断请求方式post还是get
        if(strtolower($method)=='post') {
            /**************处理post相关选项******************/
            //是否为post请求 ,处理请求数据
            curl_setopt($curl,CURLOPT_POST,true);
            curl_setopt($curl,CURLOPT_POSTFIELDS,$data);
        }
        //是否处理响应头
        curl_setopt($curl,CURLOPT_HEADER,false);
        //是否返回响应结果
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
        
        //发出请求
        $response = curl_exec($curl);
        if (false === $response) {
            echo '<br>', curl_error($curl), '<br>';
            return false;
        }
        //关闭curl
        curl_close($curl);
        return $response;
    }




}

?>