<?php
//使用微信公众号测试账号主动发提醒消息 2021.5.14 Linyufan.com
header('Content-type:text/html;charset=utf8');
require 'WxTmpLinyufan.class.php';

$username = check_str($_GET['username']);
$comm_content = check_str($_GET['comm_content']);
$comm_title = check_str($_GET['comm_title']);
$comm_time = date("Y-m-d H:i:s", time());

//下面这个是我针对我博客做的替换，因为我博客提交内容时，会自带这些@谁谁的功能
//下面程序内容的意思是，把XXX替换为空
$comm_content = str_replace('<a class=\“comment_at_admin\“>','',$comm_content);
$comm_content = str_replace('<a class=\“comment_at_user\“>','',$comm_content);
$comm_content = str_replace('</a>','',$comm_content);

$comm_content = preg_replace('/<\s*img\s+[^>]*?src\s*=\s*(.*?)\\1[^>]*?\/?\s*>/i', '', $comm_content); //删除内容中的图片
$comm_content = preg_replace("#http://([A-z0-9./-]+)#", '', preg_replace("#https://([A-z0-9./-]+)#", '', $comm_content));

if($username){
    $linyufan_comm = new WxTmpLinyufan();
    $openid = '这里是你的openid'; //这里发到谁的微信，就写谁的微信openid，写多个openid的我没测试过
    //这里的参数，要在公众平台提交的模板内容里的对应
    $linyufan_comm->send($openid,$username,$comm_title,$comm_content,$comm_time);
}





//安全过滤输入
function check_str($string, $isurl = false){
    $string = preg_replace('/[\\x00-\\x08\\x0B\\x0C\\x0E-\\x1F]/','',$string);
    $string = str_replace(array("\0","%00","\r"),'',$string);
    empty($isurl) && $string = preg_replace("/&(?!(#[0-9]+|[a-z]+);)/si",'&',$string);
    $string = str_replace(array("%3C",'<'),'<',$string);
    $string = str_replace(array("%3E",'>'),'>',$string);
    $string = str_replace(array('"',"'","\t",' '),array('“','‘',' ',' '),$string);
    return trim($string);
    }
?>